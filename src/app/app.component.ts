import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  selectedGlider: string;

  gliders = [
    {name: 'Bellatrix', id: 'SG405'},
    {name: 'Scapa', id: 'SG202'},
    {name: 'Talisker', id: 'SG606'},
    {name: 'Laphroaig', id: 'SG765'},
    {name: 'Boing-shark127', id: 'SG456'},
    {name: 'Lagavulin', id: 'SG345'}
  ];

onSelect(data) {
  console.log(data);
  this.selectedGlider = data;
}
}
